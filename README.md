# Embedded Systems 71 - Robotics

## Installation of ROS 2
### Selecting the distribution
The installation guide of ROS 2 can be found on the [official ROS2 wiki][ros2_wiki].
The first line of the wiki is as follows:

> "Multiple distributions of ROS2 are supported at a time. We recommend using the most recent release available when possible". 

As of writing this guide, the latest available distribution is "ROS 2 Foxy Fitzroy". 
The install guide can be found [here][ros2_install_foxy].

### Installing the distribution
ROS 2 is available for Linux, Mac and Windows.
For the remainder of the course, we will be using Ubuntu 20.
We now have the option to either install ROS from the binaries or by installing it from the Debian packages.

The wiki suggest to use the Debian packages, a how-to guid on the matter can be found [here][ros2_debian_archives].

#### Step 1
The first step, as explained in the [guide][ros2_debian_archives], is to setup the locale.
This can be done using the following commands:
```sh
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8
```

#### Step 2
The second step is to add the ROS 2 apt repositories. 
First you must authorize the ROS 2 GPG key, using this command:
```sh
sudo apt update && sudo apt install curl gnupg2 lsb-release
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```
Then, you must add the repository, using this command:
```sh
sudo sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'
```

#### Step 3
The next step is to install the required packages.
Prior to installing the packages, make sure to update your apt caches, using:
```sh
sudo apt update
```

Next up, you decide to either install the desktop variant or bare-bones variant of ROS 2.
For our purposes, we will be installing the desktop version as it comes pre-delivered with the following packages: OS, RViz, demos, tutorials.
To install this version enter the following command:
```sh
sudo apt install ros-foxy-desktop
```

#### Step 4
Now the environment needs to be setup, this can be achieved using the following command:
```sh
source /opt/ros/foxy/setup.bash
```
> NOTE: The extension of the setup file depends on your selected shell environment, i.e. if you are using `zsh` check for the `setup.zsh`.

To make life easier, you can install the auto completion for arguments, using the following command:
```sh
sudo apt install -y python3-pip
pip3 install -U argcomplete
```

#### Step 5

Colcon is an iteration on the ROS build tools catkin_make, catkin_make_isolated, catkin_tools and ament_tools. For more information on the design of colcon see the [documentation]("https://design.ros2.org/articles/build_tool.html").

You can install it using the following command:
```sh
sudo apt install python3-colcon-common-extensions
```

#### Step 6 (Optional)

> NOTE: In order to install the bridge make sure you have ROS 1 installed as well. Since we are using Ubuntu 20, we will be installing [ROS Noetic](#installing-noetic).

Optionally, it is possible to install ROS 1 packages and use them in ROS 2. 
To do this, you need to install the ROS 1 bridge, this can be done using the following command:
```sh
sudo apt update
sudo apt install ros-foxy-ros1-bridge
```

##### Installing Noetic
An installation guide to ROS Noetic can be found [here][ros_install_noetic]. 
The installation process is rather similar to what we have done to install ROS 2.

Add the ROS sources to your apt caches:
```sh
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```

Add the keys to the list.
```sh
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | sudo apt-key add -
```

Update your caches.
```sh
sudo apt update
```

To save time, install the desktop version of Noetic with the standard simulator packages.
```sh
sudo apt install ros-noetic-desktop-full
```

Now that Noetic is installed, you can install the [ROS 1 bridge](#step-5-optional) to make ROS 2 compatible with ROS 1.

## Getting Started with the Development of a Package
Below you will find information on how to create a package using ROS 2.
This is information is only required if you want to know how we build a package from scratch.
This section is irrelevant if you only want to run the nodes.

### Creating a new package
After ROS 2 is successfully installed, it is time to create a package.
ROS 2 provides the user with a simple command line tool to generate a package setup, 
and even to start a package with a simple node inside.
This command is as follows:
```sh
# This command will create a package 'foo' with a node 'bar' in it. 
# We have also clarified to use 'ament_cmake' as our build system. 
ros2 pkg create foo --build-type=ament_cmake --node-name bar
```

### Initializing the package
To initialize the package, switch to directory of the package and execute the following command:
```sh
colcon build
```
This will build what is currently present in your package. 
Next, you should run the setup using the following command:
```sh
# NOTE: The '.' at the start must be followed by a ' '. 
. install/setup.sh
```

### Running the package
To run a package, it must contain at least one executable, 
if you created the package with the `--node-name` flag, it should have created one automatically.
You can run the executable using the following command:
```sh
ros2 run <package-name> <executable>

# Using our example it would be 'ros2 run foo bar'
```

### Creating a launch file
Unlike ROS 1, which uses XML-based launch files, ROS 2 uses python.
An in-depth tutorial about these files can be found [here][ros2_tut_launch].

### Adding dependencies
Adding dependencies happens similarly to ROS 1.
All package dependencies must be added to the `package.xml` as a `depend`, `build_depend`, `exec_depend`, etc.

After the dependencies are added, make sure link to the dependencies correctly.
If you have selected CMake or an equivalent as your build system, 
you want to add the dependencies like this:
```cmake
# find dependencies
find_package(ament_cmake REQUIRED)

# make sure the dependency is found
find_package(<dependency> REQUIRED)


add_executable(<executable> <sources>)

    ament_target_dependencies(<executable> <dependencies>)

    target_include_directories(<executable> PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>
    )

    install(TARGETS <executable>
        DESTINATION lib/${PROJECT_NAME}
)
```

### Updating and installing dependencies
The colcon build system provided by ROS 2 usually updates and manages the dependencies of the project automatically.
At the cost of a longer built time, you need not worry about installing the correct versions of libraries.
If, during the compilation, it does not manage the dependencies automatically, 
you will need to perform the following steps manually.

Before you can update the dependencies required by the `package.xml`, 
you must first initialize the dependency cache.
You can do this using the following command:
```sh
rosdep init
```

To up date th dependencies use the following command:
```sh
rosdep update
```

### Linting with VScode
To make the development cycle easier,
make sure you do not forget to configure linter.
Since we will be using visual studio code, 
we will need to add a file to the `.vscode` directory in the project root.
In here we will create a file named `c_cpp_properties.json` with the following content:
```json

{
    "configurations": [
        {
            // Add a configuration for 'linux'
            "name": "Linux",
            "includePath": [
                "${workspaceFolder}/**",
                "/opt/ros/foxy/include/"
            ],
            "intelliSenseMode": "gcc-x64",
            "compilerPath": "/usr/bin/gcc",
            "cStandard": "gnu99",
            "cppStandard": "gnu++14"
        }
    ],
    "version": 4
}
```

[ros2_wiki]: https://index.ros.org/doc/ros2/Installation/#installationguide 

[ros2_install_foxy]: https://index.ros.org/doc/ros2/Installation/Foxy/
[ros_install_noetic]: https://wiki.ros.org/Installation/Ubuntu?distro=noetic
[ros2_debian_archives]: https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/

[ros2_tut_launch]: https://index.ros.org/doc/ros2/Tutorials/Launch-Files/Creating-Launch-Files/