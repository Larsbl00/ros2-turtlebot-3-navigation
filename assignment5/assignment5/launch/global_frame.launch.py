from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(
            package='tf2_ros',
            name='map_to_odom',
            executable='static_transform_publisher',
            arguments='0.0 0.0 0.0 0 0 0 1 map odom'.split(' ')
        ),
        Node(
            package='tf2_ros',
            name='odom_to_base_link',
            executable='static_transform_publisher',
            arguments='0.0 0.0 0.0 0 0 0 1 odom base_link'.split(' ')
        ),

    ])
