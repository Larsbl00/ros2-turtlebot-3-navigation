import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir
from launch.actions import ExecuteProcess
from launch.substitutions import LaunchConfiguration


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='True')

    shared_dir = get_package_share_directory('assignment5')
    launch_dir = os.path.join(shared_dir, 'launch')
    world_dir = os.path.join(shared_dir, 'worlds')
    map_dir = os.path.join(shared_dir, 'maps')

    # world_file = os.path.join(world_dir, 'empty_world.model')
    world_file = os.path.join(
        get_package_share_directory('turtlebot3_gazebo'),
        'worlds',
        'turtlebot3_houses',
        'waffle.model'
    )

    map_file = os.path.join(
        map_dir,
        'map.yaml'
    )

    return LaunchDescription([

        ExecuteProcess(
            cmd=['ros2', 'param', 'set', '/gazebo',
                 'use_sim_time', use_sim_time],
            output='screen'),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [launch_dir, '/robot_state_publisher.launch.py']),
            launch_arguments={'use_sim_time': use_sim_time}.items(),
        ),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [launch_dir, '/global_frame.launch.py'])
        ),

        ExecuteProcess(
            cmd=['gazebo', '--verbose', world_file,
                 '-s', 'libgazebo_ros_init.so'],
            output='screen'),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                get_package_share_directory('nav2_bringup'),
                '/launch/bringup_launch.py'
            ]),
            launch_arguments=[('map', map_file)]
        ),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                get_package_share_directory('nav2_bringup'),
                '/launch/rviz_launch.py'
            ])
        ),

        Node(
            package='assignment5',
            namespace='assignment5',
            executable='move_path_node',
            name='move_path'
        )
    ])
