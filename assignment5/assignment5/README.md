# Assignment 5/6 - Actions & Navigation
To reach a goal, like moving  its base along a trajectory to a new position, or moving its arm to a new pose, the robot needs quite some time. 
Often the robot has to pursue multiple goals in parallel. 
The purpose of this assignment is to get you acquainted with actionlib. 
This ROS library allows you to write a node that can manage one or more long-running actions implemented by other nodes.

Also, the purpose of this assignment is to get acquainted with the navigation stack. 
The navigation stack serves to drive a mobile base autonomously from one location to another while safely avoiding obstacles.

As discussed in a meeting with Punter, Teade H.T., 
due to the turtlebot 3 not providing the action-client server and we have no way to substitute this,
we will need to program this ourselves. 
This requires us to make use of the navigation stack to solve the issue.
Resulting in us combining both assignments.

As such, we will provide the following:
- We will create a launch file where the user can remotely control the turtlebot.
  While the turtlebot is driving around, it will generate a map of the area.
- We will create a second launch file, which will make the robot drive through a set of way-points, 
  using actions.

## Installing Dependencies

### Control
In order to control the turtlebot remotely, 
the assignment wants us to install `arbotix`.
However, this library is not required when using turtlebot 3,
as it comes delivered with a teleops node.
This node is called `turtlebot3_teleop` and can be launched with the following command:
```sh
ros2 run turtlebot3_teleop teleop_keyboard
```

### Mapping & Localization

In the 'getting ready' section of assignment 6, 
four libraries are mentioned that we need to install.
These are `gmapping`, `acml`, `move-base` and `map-server`.
Most of these are not available for ROS2,
as such we will need to substitute the libraries:
- gmapping      --> [navigation2][lib_navigation2]
- amcl          --> [navigation2][lib_navigation2]
- move-base     --> [navigation2][lib_navigation2]
- map-server    --> [navigation2][lib_navigation2]

In ROS2 all of these libraries, 
can be replaced by Navigation2.
To install Navigation2, assuming you are using Ubuntu 20.04 or greater,
use the following command to install the dependencies:
```sh
sudo apt-get install ros-${ROS_DISTRO}-navigation2 ros-${ROS_DISTRO}-nav2-bringup "~ros-${ROS_DISTRO}-turtlebot3-.*" 
```

> If you are using a different version, please consult: <https://navigation.ros.org/build_instructions/index.html>

We have also looked at [Google's cartographer library][lib_cartographer_ros] to generate maps.
This library is more complex and has more options than Navigation2. 
Though, after looking through the requirements of the assignment, 
we decided to use Navigation2 for mapping.

### Simulation
For simulation purposes we will be making use of both RVIZ and Gazebo.
We will be using Gazebo to test our robot in a simulation. 
And we will be using RVIZ to test our navigation,
as it comes with some simple tools to create a path to traverse and test our navigator.
As such, you will need to install both RVIZ and Gazebo. 
If you installed the full ROS2 desktop experience as recommended, 
you will not need to install the programs.

## How does it work
<!-- Explain how the apps works -->
Since we have combined both assignment 5 and 6, as explained in [Assignment 5/6 - Actions & Navigation](#assignment-56-actions-navigation),
we will explain how the application works in move triangle mode, teleops mode, and autonomous mode.

### Move Path
This part of the assignment is inherited from assignment 5.
The move path node is responsible for parsing its input message at `/cmd` and
will use the provided data in order to drive around.
At `/cmd` this node expects to receive a list of points.
The node will then travel to these points in the same order without hitting any obstacles.

As this part is inherited from assignment 5, 
we will need to make use of actions. 
To do this, 
this node will use the `/FollowPath` action server provided by navigation2.
In the completion, feedback and results handlers of our action client,
we will log bits of the received information.

### Mapper
The Mapper launch file (`mapper.launch.py`) will launch the gazebo, RVIZ and other modules required to generate a map.
Gazebo will be launched to simulate the environment.
RVIZ will be used to display the map gathered by the turtlebot.
The next step is to start the `turtlebot3_teleop teleop_keyboard` node as it cannot be ran from a launch file. The following command can be used to start this teleop node `ros2 run turtlebot3_teleop teleop_keyboard`. When this node is ran, it will print the controls for the teleop to the terminal.

To start mapping, make sure that the `mapper.launch.py` launch file and the `turtlebot3_teleop teleop_keyboard` node are running. When these are running one can move the turtlebot by selecting the `turtlebot3_teleop teleop_keyboard` terminal and using the buttons/keys it printed to the terminal.

When we are done, we can export the generated map using `ros2 run nav2_map_server map_saver_cli -f <map_name>`. When a map is generated and saved, we can load it by parsing it as an argument to the `nav2_bringup bringup_launch.py` launch file when driving in autonomous mode.

### Autonomous Mode
While working on the navigation, we decided to simulate the turtlebot in RVIZ.
While we were doing so, we noticed some world information was missing.
The turtlebot only has a base_link and as visible in *figure 1* below and 
[this](https://answers.ros.org/question/9928/transform-from-base_link-to-map-navigation-stack-error/) item on the ROS forums, we will need to setup a global frame.

![](./img/ros_global_frame.png)
> Figure 1: ROS global frame

In order to fix this we will need to use static transform publishers, 
to connect our `base_link` to `odom` and our `odom` to `map`.

#### Creating a test setup
To simulate the robot and test the mapping, we will need to run RVIZ and Gazebo parallel to each other.
For our simulation we will be using the `turtlebot3_world.launch.py` file.
More info on the world can be found here: <https://emanual.robotis.com/docs/en/platform/turtlebot3/ros2_simulation/#2-turtlebot3-world>.
This world creates a hexagon-shaped map with 9-evenly spaced beams placed in the center.
After everything is set and configured properly, it should look like *figure 2* and *figure 3*.

![](./img/navigation_world_gazebo.png)
> Figure 2: Navigation in Gazebo

![](./img/navigation_world_rviz.png)

> Figure 3: Navigation in RVIZ

In order to launch this setup correctly, 
the following things need to happen in order and in different terminals (do make sure they are sourced with ROS):

1. Link the `map` to `odom`:
```sh
ros2 run tf2_ros static_transform_publisher 0.0 0.0 0.0 0 0 0 1 map odom
```

2. Link the `odom` to `base_link`:
```sh
ros2 run tf2_ros static_transform_publisher 0.0 0.0 0.0 0 0 0 1 odom base_link
```

3. Launch gazebo with the `turtlebot3_worlds` map
```sh
gazebo -s libgazebo_ros_init.so ~/turtlebot3_ws/src/turtlebot3/turtlebot3_simulations/turtlebot3_gazebo/worlds/turtlebot3_worlds/waffle.model
```

4. Launch navigation and localization stack
```sh
ros2 launch nav2_bringup bringup_launch.py
```

5. Run RVIZ with a map view configuration (provided by nav2_bringup)
```sh
ros2 launch nav2_bringup rviz_launch.py
```


> Note: These actions will be performed automatically *and* in order when the provided launch files are used.

To test if everything is configured correctly,
Gazebo and RVIZ should contain a similar map.
If both are loaded correctly,
you are able to create way-points in RVIZ for the robot to follow.
To do this, follow these steps:
1. Press the `Waypoint mode` button in the bottom-left corner.
2. Press the `Navigation2 goal` in the top bar.
3. Create a way-point by clicking somewhere on the map. 
   Once pressed, drag it into a direction in which the robot should look.
4. To create more way-points repeat the process from *step 2* as many times as you want.
5. Once all way-points have been created,
   press the `Start Navigation` button in the bottom right corner.

If everything is done correctly, 
the robot should be moving in both RVIZ and Gazebo.

If it does not do this,
a couple of things might be wrong.

The first thing you should check are the navigation and localization status.
Within the `Navigation2` section of RVIZ, 
the `Navigation` and `Localization` labels should set to `active`.
If this is not the case, something went wrong launching the navigation and localization modules.

If everything is connected, 
but the robot still does not move.
It might be that the robot is placed in close proximity to an object.

We have not ran into any other issues.

#### Creating a new setup

After learning how to create our test setup,
we wanted to created a new setup.
This is because the test-setup already came with a map loaded into RVIZ.

Thus, we decided to load the [turtlebot3_houses](https://emanual.robotis.com/docs/en/platform/turtlebot3/ros2_simulation/#3-turtlebot3-house) map into Gazebo.
We then drove around this place using the mapper launch file and the turtlebot teleops module.
While driving around, 
our map loaded into RVIZ. 
Now, we are able to export this map using the following command:
```sh
ros2 run nav2_map_server map_saver_cli -f ./map
```
This will create two files in the directory of execution; 
it will create a file named `map.pgm` and a file named `map.yaml`.

> Note: Do make sure the robot is continuously updating the map while running. If the saver does not detect any changes, it will not save the map. 

To load the map,
we will have to include the launch description of the `bringup_launch.py` provided by `nav2_bringup` 
and provided it with our `map.yaml` file.

## Limitations
In this section, we will go over some of the limitations we have run into.

### Not using a roomba to generate maps
In the assignment we are supposed to create two different maps of the same world.
One using a turtlebot and one with a roomba. 
Unfortunately, we will not be able to use a roomba, 
as the robot is not available for ROS 2.


## Results


### Mapper
To be able to use the mapper, one must run the `./assignment5/launch/mapper.launch.py` launch file and the `turtlebot3_teleop teleop_keyboard` node. For this, the following commands can be use:

```sh
ros2 launch ./assignment5/launch/mapper.launch.py
```

Then, you must run the teleop from a seconded *sourced* terminal.

```sh
ros2 run turtlebot3_teleop teleop_keyboard
```

#### Expectations
After starting the afore mentioned processes, we expect that the turtlebot can be controlled using the `turtlebot3_teleop teleop_keyboard` node.
As the robot moves around by the input of the user, we expect the robot to gradually update the map (at topic: `/map`) which should eventually create a map of the whole area.

#### Results

When everything is started, it will start with a simple map that only contains everything that is in sight of the LIDAR.
After the robots movement was initiated, it will start extending the map to create a larger and more accurate map of the area.
The progress of the mapper can be found in *figure 4*, *figure 5* and *figure 6*. These figures show the mapping process from 'before movement is started' to 'after a while of driving' and shows that our mapper is working as expected.

![](./img/mapper_rviz_1st.png)
> Figure 4: Mapper before movement is started

![](./img/mapper_rviz_2nd.png)
> Figure 5: Mapper after driving for a couple of meters

![](./img/mapper_rviz_3d.png)
> Figure 6: Mapper after a while of driving

### Move Path

To test the `move_path_node` we have made a new node.
The node called 'path_publisher_node' is used to publish a set of interesting points to our move_path_node.
This note was created because the ROS2 CLI is not to keen on complex and nested data structures.
As such, you will first need to launch the move_path_node using:

```sh
ros2 launch ./assignment5/launch/move_path.launch.py
```

Then, you must run the path publisher from a seconded *sources* terminal.

```sh
ros2 run path_publisher path_publisher_node
```

#### Expectation
After using the `path_publisher_node` to publish a path to our `move_path_node`,
we expect that the `move_path_node` guides the turtlebot through the given path, without hitting any obstacles.
As the robot approaches one of the checkpoints, 
we expect the robot to calculate a route to the next checkpoint.

While the robot is travelling between points, 
we expect that the terminal from which the `move_path_node` is launched will print the following things:
- Response: log messages will display when a new goal is accepted by the server or not.
- Feedback: log messages will display the current waypoint.
- Result: log messages will display whether the action ended successfully or not.

#### Results

When a goal has been accepted by the server and it has calculated the next position,
we can see how the robot should navigate between the points.
If we look at *figure 7* and *figure 8*,
we can see that the path of the robot is displayed on top of the map.

![](./img/path_moving_1st_tgt.png)
> Figure 7: Path displaying 1st target

![](./img/path_moving_3rd_tgt.png)
> Figure 8: Path displaying 3rd target

*Figure 9*, *Figure 10* and *Figure 11* highlight the different types of logged messages as a response to the action client.
The responses also contain the expected type of response.
When a goal is reached, a success message is displayed.

![](./img/action_goal_reached.png)
> Figure 9: Action message - reached goal

While the robot is moving between waypoints,
a message is received as feedback. 
Said message displays the waypoint the robot is currently at.

![](./img/action_feedback.png)
> Figure 10: Action message - received feedback

Once all waypoints are reached a message is display indicating that is has completed all waypoints.

![](./img/action_completed.png)
> Figure 11: Action message - completed action



[lib_cartographer_ros]: https://github.com/ros2/cartographer_ros
[lib_navigation2]: https://github.com/ros-planning/navigation2