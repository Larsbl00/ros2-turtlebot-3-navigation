/**
 * @file move_path_node.hpp
 * @brief
 * @version 0.1
 * @date 2020-10-07
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef ASSIGNMENT5_MOVE_PATH_MOVE_PATH_NODE_HPP
#define ASSIGNMENT5_MOVE_PATH_MOVE_PATH_NODE_HPP

/////////////////////
// INCLUDES
/////////////////////

#include <cmath>
#include <iostream>
#include <memory>
#include <vector>

#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/client.hpp>
#include <rclcpp_action/create_client.hpp>

#include <geometry_msgs/msg/pose_array.hpp>
#include <nav2_msgs/action/follow_waypoints.hpp>
#include <nav_msgs/msg/path.hpp>

/////////////////////
// CLASSES
/////////////////////

// follow_path

namespace assignment5::move_path
{

class MovePathNode : public rclcpp::Node
{

  public:
    MovePathNode();

    ~MovePathNode() noexcept = default;

  private:
    nav_msgs::msg::Path goal_;
    rclcpp_action::Client<nav2_msgs::action::FollowWaypoints>::SendGoalOptions
        follow_waypoints_goal_options_;

    rclcpp::Subscription<geometry_msgs::msg::PoseArray>::SharedPtr goal_subscription_;

    rclcpp_action::Client<nav2_msgs::action::FollowWaypoints>::SharedPtr
        navigation_waypoints_client_;

    /**
     * @brief Callback for the `/goal` topic
     *
     * @param msg
     */
    void goal_callback_(const geometry_msgs::msg::PoseArray::SharedPtr msg);

    /**
     * @brief Callback when `/FollowWaypoint` sends a response
     *
     * @param future
     */
    void follow_waypoints_response_callback_(
        std::shared_future<
            rclcpp_action::ClientGoalHandle<nav2_msgs::action::FollowWaypoints>::SharedPtr>
            future);

    /**
     * @brief Callback when `/FollowWaypoint` sends feedback
     *
     * @param handle
     * @param feedback
     */
    void follow_waypoints_feedback_callback_(
        rclcpp_action::ClientGoalHandle<nav2_msgs::action::FollowWaypoints>::SharedPtr handle,
        nav2_msgs::action::FollowWaypoints::Feedback::ConstSharedPtr feedback);

    /**
     * @brief Callback when `/FollowWaypoint` sends result
     *
     * @param result
     */
    void follow_waypoints_result_callback_(
        const rclcpp_action::ClientGoalHandle<nav2_msgs::action::FollowWaypoints>::WrappedResult
            &result);
};

} // namespace assignment5::move_path

#endif