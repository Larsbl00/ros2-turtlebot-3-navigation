/**
 * @file move_path_node.cpp
 * @brief
 * @version 0.1
 * @date 2020-10-07
 *
 * @copyright Copyright (c) 2020
 *
 */
#include "move_path/move_path_node.hpp"

namespace assignment5::move_path
{

MovePathNode::MovePathNode() : rclcpp::Node("move_path_node")
{
    this->goal_subscription_ = this->create_subscription<geometry_msgs::msg::PoseArray>(
        "/cmd", 20, std::bind(&MovePathNode::goal_callback_, this, std::placeholders::_1));

    this->navigation_waypoints_client_ =
        rclcpp_action::create_client<nav2_msgs::action::FollowWaypoints>(this, "/FollowWaypoints");

    if (!this->navigation_waypoints_client_->wait_for_action_server(std::chrono::seconds(20)))
    {
        RCLCPP_FATAL(this->get_logger(), "Could not load action server");
    }
    RCLCPP_INFO(this->get_logger(), "Connected to action server '/follow_waypoints'");

    this->follow_waypoints_goal_options_.goal_response_callback =
        std::bind(&MovePathNode::follow_waypoints_response_callback_, this, std::placeholders::_1);

    this->follow_waypoints_goal_options_.feedback_callback =
        std::bind(&MovePathNode::follow_waypoints_feedback_callback_, this, std::placeholders::_1,
                  std::placeholders::_2);

    this->follow_waypoints_goal_options_.result_callback =
        std::bind(&MovePathNode::follow_waypoints_result_callback_, this, std::placeholders::_1);
}

void MovePathNode::goal_callback_(const geometry_msgs::msg::PoseArray::SharedPtr msg)
{

    RCLCPP_INFO(this->get_logger(), "Received waypoints");

    nav2_msgs::action::FollowWaypoints::Goal goal;

    for (auto &pose : msg->poses)
    {
        geometry_msgs::msg::PoseStamped ps;
        ps.header.frame_id = "map";
        ps.pose = pose;

        RCLCPP_INFO(this->get_logger(), "Frame: %s; {%f, %f, %f} | {%f, %f, %f}",
                    ps.header.frame_id.c_str(), ps.pose.position.x, ps.pose.position.y,
                    ps.pose.position.z, pose.position.x, pose.position.y, pose.position.z);

        goal.poses.push_back(std::move(ps));
    }

    this->navigation_waypoints_client_->async_send_goal(goal, this->follow_waypoints_goal_options_);
}

void MovePathNode::follow_waypoints_response_callback_(
    std::shared_future<
        rclcpp_action::ClientGoalHandle<nav2_msgs::action::FollowWaypoints>::SharedPtr>
        future)
{
    auto goal_handle = future.get();
    if (!goal_handle)
    {
        RCLCPP_ERROR(this->get_logger(), "Follow waypoints response: Goal was rejected by server");
    }
    else
    {
        RCLCPP_INFO(this->get_logger(),
                    "Follow waypoints response: Goal accepted by server, waiting for result");
    }
}

void MovePathNode::follow_waypoints_feedback_callback_(
    rclcpp_action::ClientGoalHandle<nav2_msgs::action::FollowWaypoints>::SharedPtr handle,
    nav2_msgs::action::FollowWaypoints::Feedback::ConstSharedPtr feedback)
{
    (void)handle;
    RCLCPP_INFO(this->get_logger(), "Follow waypoints feedback: { 'current_waypoint': %i }",
                feedback->current_waypoint);
}

void MovePathNode::follow_waypoints_result_callback_(
    const rclcpp_action::ClientGoalHandle<nav2_msgs::action::FollowWaypoints>::WrappedResult
        &result)
{
    switch (result.code)
    {
    case rclcpp_action::ResultCode::SUCCEEDED:
        RCLCPP_INFO(this->get_logger(), "Follow waypoints result: Goal was succeeded");
        break;
    case rclcpp_action::ResultCode::ABORTED:
        RCLCPP_ERROR(this->get_logger(), "Follow waypoints result: Goal was aborted");
        return;
    case rclcpp_action::ResultCode::CANCELED:
        RCLCPP_ERROR(this->get_logger(), "Follow waypoints result: Goal was canceled");
        return;
    default:
        RCLCPP_ERROR(this->get_logger(), "Follow waypoints result: Unknown result code");
        return;
    }
}

} // namespace assignment5::move_path

/////////////////////
// FUNCTIONS
/////////////////////

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    rclcpp::spin(std::make_shared<assignment5::move_path::MovePathNode>());

    rclcpp::shutdown();

    return 0;
}