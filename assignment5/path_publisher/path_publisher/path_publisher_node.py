import rclpy
from rclpy.node import Node

from std_msgs.msg import Header
from geometry_msgs.msg import PoseArray, Pose


class PathPublisher(Node):
    def __init__(self):
        super().__init__('path_publisher')

        poses = PoseArray()

        # Create pose and link it to frame_id map
        pose = Pose()
        pose.position.x = 6.
        pose.position.y = -1.
        poses.poses.append(pose)

        pose = Pose()
        pose.position.x = 1.
        pose.position.y = 3.
        poses.poses.append(pose)

        pose = Pose()
        pose.position.x = -6.5
        pose.position.y = -2.
        poses.poses.append(pose)

        pose = Pose()
        pose.position.x = 1.
        pose.position.y = -2.
        poses.poses.append(pose)

        self.create_publisher(PoseArray, '/cmd', 1).publish(poses)


def main(args=None):
    rclpy.init(args=args)

    rclpy.spin_once(PathPublisher())

    rclpy.shutdown()


if __name__ == "__main__":
    main()
